module "container_stack" {
  source          = "./container_stack"
  container_image = "httpd"
  container_count = 5
}
