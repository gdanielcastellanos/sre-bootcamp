variable "container_count" {
  description = "Number of containers to deploy"
}

variable "container_image" {
  description = "Name of the image in dockerhub"
}

variable "network_name" {
  description = "Name of the container stack network"
  default = "container_stack_default"
}

variable "network_driver" {
  description = "Network driver type"
  default = "bridge"
}