resource "docker_network" "container_stack_network" {
  name = "${var.network_name}"
  driver = "${var.network_driver}"
}

resource "docker_image" "container_image" {
  name         = var.container_image
  keep_locally = false
}

resource "docker_container" "container" {
  count = var.container_count
  image = docker_image.container_image.name
  name  = "${docker_image.container_image.name}-${count.index}"

  networks_advanced {
    name = docker_network.container_stack_network.name
  }
}