variable "instance_name" {
  default     = "kontinu-test-instance"
  description = "The name of the instance to create."
  type        = string
}


variable "instance_type"{
  default     =  "t2.micro"
  description = "The type of instance to create."
  type        = string
}
