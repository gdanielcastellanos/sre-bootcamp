import time, os
i=0

envvar=os.getenv("ENVVAR", "default")


while True:
  print(f"[{i}] Hello from Python 🐍 {envvar} \n")
  time.sleep(1)
  i+=1
