# In your terminal

```bash
git remote add gitlab git@gitlab.com:kontinu/sre-bootcamp.git
git pull gitlab main

# to synchronize changes to your remote
git push
```

# To keep updating your fork

```bash
git pull gitlab main
```
